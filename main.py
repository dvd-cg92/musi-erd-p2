from DatabaseFactory import DatabaseFactory
from DataTableFactory import DataTableFactory
import random
import json
import names

sqlFileTarget = ["tableDefinitions", "federatedTableDefinitions"]
species = ["gato", "perro", "cabra", "gallina", "tritón", "canario"]
sexChoices = ["femenino", "masculino"]

# Fill the sql definitions with the user configuration
def replaceConfigurations(sql, schemaConfigurations):
    sql = sql.replace("{{main_user}}", schemaConfigurations["main"]["user"])
    sql = sql.replace("{{main_password}}", schemaConfigurations["main"]["password"])
    sql = sql.replace("{{main_host}}", schemaConfigurations["main"]["address"] + ":" + schemaConfigurations["main"]["port"])
    sql = sql.replace("{{main_database}}", schemaConfigurations["main"]["dbName"])
    sql = sql.replace("{{f1_user}}", schemaConfigurations["federated1"]["user"])
    sql = sql.replace("{{f1_password}}", schemaConfigurations["federated1"]["password"])
    sql = sql.replace("{{f1_host}}", schemaConfigurations["federated1"]["address"] + ":" + schemaConfigurations["federated1"]["port"])
    sql = sql.replace("{{f1_database}}", schemaConfigurations["federated1"]["dbName"])

    return sql

f = open('settings.json')

configurationsJson = json.load(f)
schemaConfigurations = configurationsJson["schema"]

# Creates a database for every configuration and the corresponding tables
for key in schemaConfigurations:
    config = schemaConfigurations[key]
    print("Creating database " + config["dbName"])
    dbFactory = DatabaseFactory(config["address"], config["port"], config["user"], config["password"], config["dbName"])
    dbFactory.createDatabase()
    tableDefinitionFile = config["tableDefinitions"]
    sql_file = open(tableDefinitionFile)
    sql_as_string = sql_file.read()
    sql_as_string = replaceConfigurations(sql_as_string, schemaConfigurations)
    dtFactory = DataTableFactory
    print("Creating tables from " + config["dbName"])
    dtFactory.CreateDataTables(dbFactory, sql_as_string)

# Fill randomly the tables
config = schemaConfigurations["federated1"]
dbFactory = DatabaseFactory(config["address"], config["port"], config["user"], config["password"], config["dbName"])
dtFactory = DataTableFactory

# Location
print("Filling the 'localizacion' table")
for x in range(1, configurationsJson["totalLocations"] + 1):
    street = names.get_first_name() + " St."
    number = random.randint(0, 99)
    floor = random.randint(0, 10)
    location = names.get_last_name()
    latitude = random.random()
    longitude = random.random()
    dtFactory.InsertLocation(dbFactory, street, number, floor, location, latitude, longitude)

config = schemaConfigurations["main"]
dbFactory = DatabaseFactory(config["address"], config["port"], config["user"], config["password"], config["dbName"])
dtFactory = DataTableFactory

# Person
print("Filling the 'persona' table")
for x in range(1, configurationsJson["totalPeople"] + 1):
    surname = names.get_last_name()
    name = names.get_first_name()
    sex = random.choice(list(sexChoices))
    age = random.randint(0, 99)
    dtFactory.InsertPerson(dbFactory, name, surname, age, sex)

# Pet
print("Filling the 'mascota' table")
for x in range(1, configurationsJson["totalAnimals"] + 1):
    animalType = random.choice(list(species))
    name = names.get_first_name()
    sex = random.choice(list(sexChoices))
    age = random.randint(0, 15)
    dtFactory.InsertPet(dbFactory, name, animalType, age, sex)

# Clinic
print("Filling the 'clinic' table")
for x in range(1, configurationsJson["totalClinics"] + 1):
    name = names.get_first_name()
    locationId = random.randint(1, configurationsJson["totalLocations"])
    dtFactory.InsertClinic(dbFactory, name, locationId)

# Pet_Person
print("Filling the 'persona_mascota' table")
for animalId in range(1, configurationsJson["totalAnimals"] + 1):
    personId = random.randint(1, configurationsJson["totalPeople"])
    dtFactory.InsertPetPersonRelation(dbFactory, animalId, personId)

for personId in range(1, configurationsJson["totalPeople"] + 1):
    animalId = random.randint(1, configurationsJson["totalAnimals"])
    dtFactory.InsertPetPersonRelation(dbFactory, animalId, personId)

print("Filling the 'mascota_clinica' table")
# Pet_Clinic
for animalId in range(1, configurationsJson["totalAnimals"] + 1):
    clinicId = random.randint(1, configurationsJson["totalClinics"])
    dtFactory.InsertPetClinicRelation(dbFactory, animalId, clinicId)