DROP TABLE IF EXISTS `localizacion`;
CREATE TABLE `localizacion` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `calle` VARCHAR(45) NULL,
  `numero` INT NULL,
  `piso` INT NULL,
  `localidad` VARCHAR(45) NULL,
  `latitud` DOUBLE NULL,
  `longitud` DOUBLE NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)) ENGINE=FEDERATED
CONNECTION='mysql://{{f1_user}}:{{f1_password}}@{{f1_host}}/{{f1_database}}/localizacion';


DROP TABLE IF EXISTS `clinica`;
CREATE TABLE `clinica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `localizacion_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `localizacion_idx` (`localizacion_id`)
) ENGINE=InnoDB
DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `mascota`;
CREATE TABLE `mascota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `especie` varchar(45) DEFAULT NULL,
  `edad` varchar(45) DEFAULT NULL,
  `sexo` enum('masculino','femenino') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB
DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `mascota_clinica`;
CREATE TABLE `mascota_clinica` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `mascota_id` int(11) DEFAULT NULL,
  `clinica_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `clinica_id_idx` (`clinica_id`),
  CONSTRAINT `clinica_FK` FOREIGN KEY (`clinica_id`)
        REFERENCES `clinica` (`id`)
        ON DELETE CASCADE,
  KEY `mascota_id_idx` (`mascota_id`),
  CONSTRAINT `mascota_FK` FOREIGN KEY (`mascota_id`)
        REFERENCES `mascota` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB
DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `persona`;
CREATE TABLE `persona` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `sexo` enum('masculino','femenino','otro') DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB
DEFAULT CHARSET=utf8;


DROP TABLE IF EXISTS `persona_mascota`;
CREATE TABLE `persona_mascota` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `persona_id` int(11) DEFAULT NULL,
  `mascota_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `persona_id_idx` (`persona_id`),
  CONSTRAINT `persona_FK` FOREIGN KEY (`persona_id`)
        REFERENCES `persona` (`id`)
        ON DELETE CASCADE,
  KEY `mascota_id_idx` (`mascota_id`),
  CONSTRAINT `mascota2_FK` FOREIGN KEY (`mascota_id`)
        REFERENCES `mascota` (`id`)
        ON DELETE CASCADE
) ENGINE=InnoDB
DEFAULT CHARSET=utf8;
