#%%
import mysql.connector as mysql

class DatabaseFactory:

    def __init__(self, host, port, user, passwd, databaseName):
        self.host = host
        self.port = port
        self.user = user
        self.passwd = passwd
        self.databaseName = databaseName
        
    def createDatabase(self):
        conn = self.getConnectionToServer()
        cursor = conn.cursor()
        cursor.execute("DROP DATABASE IF EXISTS " + self.databaseName)
        cursor.execute("CREATE DATABASE " + self.databaseName + " DEFAULT CHARACTER SET utf8")
        cursor.close()
        conn.close()

    def getConnectionToServer(self):
        return mysql.connect(
            host = self.host,
            port = self.port,
            user = self.user,
            passwd = self.passwd,
            auth_plugin = 'mysql_native_password'
        )
        
    def getConnection(self):
        return mysql.connect(
            host = self.host,
            port = self.port,
            user = self.user,
            passwd = self.passwd,
            database = self.databaseName,
            auth_plugin = 'mysql_native_password'
        )
    