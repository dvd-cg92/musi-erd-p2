class DataTableFactory:

    def CreateDataTables(db, sql):
        conn = db.getConnection()
        cursor = conn.cursor()
        for _ in cursor.execute(sql, multi=True): pass
        cursor.close()
        conn.close()

    def InsertPet(db, name, animalType, age, sex):
        conn = db.getConnection()
        cursor = conn.cursor()
        sql = "INSERT INTO " + str(conn.database) + ".`mascota` (`nombre`, `especie`, `edad`, `sexo`) " \
            "VALUES ('" + name + "', '" + animalType + "', '" + str(age) + "', '" + sex + "');"
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()

    def InsertLocation(db, street, number, floor, location, latitude, longitude):
        conn = db.getConnection()
        cursor = conn.cursor()
        sql = "INSERT INTO " + str(conn.database) + ".`localizacion` (`calle`, `numero`, `piso`, `localidad`, `latitud`, `longitud`) " \
            "VALUES ('" + street + "', '" + str(number) + "', '" + str(floor) + "', '" + location + "', '" + str(latitude) + "', '" + str(longitude) + "');"
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()

    def InsertPerson(db, name, surname, age, sex):
        conn = db.getConnection()
        cursor = conn.cursor()
        sql = "INSERT INTO " + str(conn.database) + ".`persona` (`nombre`, `apellidos`, `edad`, `sexo`) " \
            "VALUES ('" + name + "', '" + surname + "', '" + str(age) + "', '" + sex + "');"
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()

    def InsertClinic(db, name, locationId):
        conn = db.getConnection()
        cursor = conn.cursor()
        sql = "INSERT INTO " + str(conn.database) + ".`clinica` (`nombre`, `localizacion_id`) VALUES ('" + name + "', '" + str(locationId) + "');"
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()

    def InsertPetPersonRelation(db, animalId, personId):
        conn = db.getConnection()
        cursor = conn.cursor()
        sql = "INSERT INTO " + str(conn.database) + ".`persona_mascota` (`persona_id`, `mascota_id`) VALUES ('" + str(personId) + "', '" + str(animalId) + "');"
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()

    def InsertPetClinicRelation(db, animalId, clinicId):
        conn = db.getConnection()
        cursor = conn.cursor()
        sql = "INSERT INTO " + str(conn.database) + ".`mascota_clinica` (`clinica_id`, `mascota_id`) VALUES ('" + str(clinicId) + "', '" + str(animalId) + "');"
        cursor.execute(sql)
        conn.commit()
        cursor.close()
        conn.close()